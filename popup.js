var mesNoms = {};

function finishSetNoms()
{
	showMessage("#successmessage");
	chrome.storage.sync.set({"noms" : mesNoms});
}

function majNom(ancienNom, nouveauNom)
{
	if(ancienNom in mesNoms || (!(ancienNom in mesNoms) && !valeurNomExiste(mesNoms, ancienNom))) //L'ancien nom est bien le nom d'origine, ou alors l'ancien nom n'est pas dans l'array (nouveau renommage
	{
		if(ancienNom != nouveauNom)
		{
			mesNoms[ancienNom] = nouveauNom;
			finishSetNoms();
		}
		else
		{
			chrome.tabs.query({"windowId":chrome.windows.WINDOW_ID_CURRENT, "active":true}, function(tabs){
				chrome.tabs.sendMessage(tab.id, {noms: mesNoms});
				delete mesNoms[ancienNom];
				finishSetNoms();
			});
		}
	}
	else if(valeurNomExiste(mesNoms, ancienNom)) //L'ancien nom est lui-même une valeur utilisateur
	{
		if(getCle(mesNoms, ancienNom) != nouveauNom)
		{
			mesNoms[getCle(mesNoms, ancienNom)] = nouveauNom;
			finishSetNoms();
		}
		else
		{
			var cle = getCle(mesNoms,ancienNom);
			chrome.tabs.query({"windowId":chrome.windows.WINDOW_ID_CURRENT, "active":true}, function(tabs){
				var tab = tabs[0];
				chrome.tabs.sendMessage(tab.id, {noms: mesNoms});
				answer = true;
				delete mesNoms[cle];
				finishSetNoms();
			});
		}
	}
}

function showMessage(classMessage)
{
	$(".message").removeClass("message-visible").addClass("message-invisible");
	$(classMessage).removeClass("message-invisible").addClass("message-visible");
	var i = 0;
	var intervalId = setTimeout(function(){
		clearInterval(intervalId);
		$(classMessage).removeClass("message-visible").addClass("message-invisible");
		i++;
	}, 1000);
}

function renommerOnglet(nom)
{
	$("a").click(function(){ //Emmène sur le site d'Inside EP
		chrome.tabs.create({url: $(this).attr("href")});
	});

	$("input[id='vidervalider']").click(function(){
		if($("input[type=checkbox]:checked").size() > 0)
		{
			chrome.tabs.getSelected(null, function(tab){
				chrome.storage.sync.get("noms", function(resultat){
					if(resultat.noms)
					{
						chrome.tabs.sendMessage(tab.id, {noms: resultat.noms});
						chrome.storage.sync.clear();
						showMessage("#vidermessage");
					}
				});
			});
		}
	});

	chrome.storage.sync.get("noms", function(resultat){

		if(resultat.noms) //Obtention des noms et préparation des champs
			mesNoms = resultat.noms;
		$("input[id=categorie]").val(nom[0]);
		var cle = getCleAnyway(mesNoms, nom[0]);
		$("#nom-origine").text(cle);

		$(".form").keypress(function(e){ //Validation par la touche entrée
			if(e.keyCode == 13){
				var element = $(".form").find("input[type=submit]");
				element.click();
			}
			return e;
		});

		$("input[id='renommer']").click(function(){
			var nouveauNom = $("input[id='categorie']").val();
			if(isNouveauNomValide(mesNoms, nouveauNom, nom[0]))
			{
				majNom(nom[0], nouveauNom);
			}
			else
			{
				showMessage("#errormessage");
			}
		});
	});
}


$("document").ready(function () {
	chrome.tabs.getSelected(null, function(tab) {
		var tablink = tab.url;
		if(tablink.indexOf("entg.sciences-po.fr") > -1)
		{
			$(".popup-placeholder").removeClass("popup-visible").addClass("popup-invisible");
			$(".popup-vrai").removeClass("popup-invisible").addClass("popup-visible");
			chrome.tabs.executeScript(null, {file:"myjquery.js"} );
			chrome.tabs.executeScript(null, {file:"mytemplates.js"} );
			chrome.tabs.executeScript(null, {code:"$('.ctxNameLabel').html();"}, renommerOnglet);
		}
		else
		{
			$(".popup-vrai").removeClass("popup-visible").addClass("popup-invisible");
			$(".popup-placeholder").removeClass("popup-invisible").addClass("popup-visible");
		}
	});
});


