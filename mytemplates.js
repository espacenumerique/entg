function valeurNomExiste(noms, valeur)
{
	existence = false;
	for(cle in noms)
	{
		if(noms[cle] == valeur)
			existence = true;
	}
	return existence;
}

function getCle(array, valeur)
{
	for(cle in array)
	{
		if(array[cle] == valeur)
			return cle;
	}
	return false;
}

function isNouveauNomValide(noms, nouveauNom, nom)
{
	if(nouveauNom in noms && (nouveauNom != getCle(noms, nom) || getCle(noms,nom) == false) && nouveauNom != nom) //Le nouveau nom est déjà pris par une clé qui n'est pas celle d'origine
		return false;
	if(valeurNomExiste(noms,nouveauNom) && nouveauNom != nom) //Le nouveau nom est déjà pris
		return false;
	return true;
}

function getCleAnyway(mesNoms, nomOnglet)
{
	if(valeurNomExiste(mesNoms, nomOnglet))
		return getCle(mesNoms, nomOnglet);
	return nomOnglet;
}


