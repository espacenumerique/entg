var noms = {};
var  changements = {};
$(document).ready(function(){
	chrome.storage.onChanged.addListener(function(changement, namespace){
		var anciensNoms = changement.noms.oldValue;
		var nouveauxNoms = changement.noms.newValue;
		var nomOnglet = $(".ctxNameLabel").html();
		var cle = getCle(anciensNoms, nomOnglet);
		if(cle == false)
			cle = nomOnglet;
		if(nouveauxNoms)
			$('.ctxNameLabel').html(nouveauxNoms[cle]);
		if(anciensNoms != undefined)
		{
			$.each(anciensNoms, function(cle, val){
				changements[val] = cle;
			});
		}
  	});
	document.addEventListener("DOMNodeInserted", function(event){
			if($(event.target).hasClass("co-main") || $(event.target).parent().hasClass("ctxNameLabel"))
			{
				chrome.storage.sync.get("noms", function(resultat){
					if(resultat.noms)
						noms = resultat.noms;
					if($(event.target).parent().hasClass("ctxNameLabel"))
						var nomOnglet = $(event.target).parent().html();
					else
						var nomOnglet = $(event.target).find(".ctxNameLabel").html();
					if(nomOnglet in noms)
						$('.ctxNameLabel').html(noms[nomOnglet]);
				});
			}
	});
	

	var observer = new WebKitMutationObserver(function(mutations) {
		$.each(mutations, function(numero, mutation){
			var ajouts = mutation.addedNodes;
			var listeGroupes = $(ajouts).find("table[__cidx]").find(".gwt-Label");
			$.each(listeGroupes, function(no, node){
				chrome.storage.sync.get("noms", function(resultat){ 
					if(resultat.noms)
						noms = resultat.noms;
					else
						noms = {};
					nomOnglet = $(node).text();
					cle = getCleAnyway(noms, nomOnglet);
					if(cle in noms)
					{
						$(node).text(noms[cle]);
					}
					else if(nomOnglet in changements)
					{
						$(node).text(changements[nomOnglet]);
						delete changements[nomOnglet];
					}
				});
			});
		});
	});
	observer.observe(document.getElementsByTagName("body")[0], { childList: true, subTree: true, subtree: true });


	chrome.extension.onMessage.addListener(function(message, sender, sendResponse){
		var nomOnglet = $(".ctxNameLabel").html();
		var cle = getCleAnyway(message.noms, nomOnglet);
		$(".ctxNameLabel").html(cle);
		changements[nomOnglet] = cle;
		sendResponse({answer:true});
	});
});

