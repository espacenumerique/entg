ENTG+
=====================
ENTG+ est une extension pour Google Chrome développée par Kévin Locoh pour Espace Numérique, l'association multimédia des étudiants de Sciences Po Paris (http://insideelectronicpipo.com).   
ENTG+ permet aux étudiants de renommer leurs groupes sur l'ENTG, leur espace de travail numérique. Mais depuis le passage de Sciences Po à Google Apps et l'abandon de l'ENTG, ENTG+ n'a plus d'utilité.

1\. Installation
----------------
Installez ENTG+ directement depuis le Chrome Web Store : https://chrome.google.com/webstore/detail/entg%20/chokpkninodgokphmobgdnmfjondhhdh?hl=fr.

2\. Utilisation
---------------
Référez-vous à notre tutoriel en ligne : http://www.insideelectronicpipo.com/helpdesk/article/avec-entg-redevenez-maitre-de.

2\. Licence
----------------------
ENTG+ est une extension libre de droits, publiée sous les termes de la licence du MIT (cf. le fichier License). 

